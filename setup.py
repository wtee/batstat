import setuptools

setuptools.setup(
    name="batstat",
    version="0.0.1",
    author="wtee",
    author_email="wteal@fastmail.com",
    description="A simple battery-status checker",
    url="https://codeberg/wtee/batstat",
    scripts=["batstat"],
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: Public Domain",
        "Environment :: Console",
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
    ],
    python_requires=">=3.6",
)
